// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BasicCharacter.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/BoxComponent.h"

#include "MyTestWeaponActor.generated.h"


UCLASS()
class MYPROJECT_API AMyTestWeaponActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMyTestWeaponActor(const class FObjectInitializer& ObjectInitializer);

	void SetOwningPawn(ABasicCharacter* NewOwner);
	void AttachMeshToPawn();
	void OnEquip(const AMyTestWeaponActor* LastWeapon);

private:
	UPROPERTY(VisibleDefaultsOnly, Category = Weapon)
	USkeletalMeshComponent* WeaponMesh;

	UPROPERTY(VisibleDefaultsOnly, Category = Weapon)
	class UBoxComponent* WeaponCollision;

protected:
	class ABasicCharacter* MyPawn;
	
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

//================================================
//public:부분에 추가
//데미지, 죽음
	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

	UPROPERTY(EditDefaultsOnly, Category = "MyFX")//
		UParticleSystem* HitFX;//
//끝
//================================================
};
