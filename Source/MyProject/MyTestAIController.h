// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "MyTestAIController.generated.h"

/**
 * 
 */
class UBehaviorTreeComponent;
class UBlackboardComponent;

UCLASS(config = Game)
class MYPROJECT_API AMyTestAIController : public AAIController
{
	GENERATED_BODY()

public:
	AMyTestAIController();

	virtual void BeginPlay() override;
	virtual void OnPossess(APawn* pawn) override;
	virtual void Tick(float deltaSeconds) override;
	virtual FRotator GetControlRotation()const override;
	TScriptDelegate<> sense;
	UFUNCTION()
		void OnPawnDetercted(TArray<AActor*> detectedPawns);


	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
		float aiSightRadius = 500.f;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
		float aiSightAge = 5.f;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
		float aiLoseSightRadius = aiSightRadius + 50.f;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
		float aiFieldOfView = 90.f;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
		class UAISenseConfig_Sight* sightConfig;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
		bool bIsPlayerDetected = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
		float distanceToPlayer = 0.0f;



private:
	UPROPERTY(transient)
		UBehaviorTreeComponent* behaviorComp;

	UPROPERTY(transient)
		UBlackboardComponent* blackboardComp;
};