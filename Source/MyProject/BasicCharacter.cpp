// Fill out your copyright notice in the Description page of Project Settings.

#include "BasicCharacter.h"
#include "Engine.h"
#include "MyTestWeaponActor.h"
#include "Kismet/GameplayStatics.h"

#include "Engine/World.h"

// Sets default values
ABasicCharacter::ABasicCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//================================================
	//������, ����
	/*static ConstructorHelpers::FObjectFinder<UParticleSystem>ParticleAsset(
		TEXT("ParticleSystem'/Game/StarterContent/Particles/P_Explosion.P_Explosion'"));
	HitFX = ParticleAsset.Object;*/
	//��
	//================================================

	isDuringAttack = false;

	ComboAttack_Num = 0;
	//================================================
	//������, ����
	myHealth = 0.f;
	myMaxHealth = 100.f;
	//��
	//================================================
}
//================================================
//���� ����

USkeletalMeshComponent* ABasicCharacter::GetSpesificPawnMesh() const
{
	return GetMesh();
}

FName ABasicCharacter::GetWeaponAttachPoint() const
{
	return WeaponAttachPoint;
}

void ABasicCharacter::EquipWeapon(AMyTestWeaponActor* Weapon)
{
	if(Weapon)
	{
		SetCurrentWeapon(Weapon, Currentweapon);
	}
}

void ABasicCharacter::AddWeapon(AMyTestWeaponActor* Weapon)
{
	if(Weapon)
	{
		Inventory.AddUnique(Weapon);
	}
}

void ABasicCharacter::SetCurrentWeapon(AMyTestWeaponActor* NewWeapon, AMyTestWeaponActor* LastWeapon)
{
	AMyTestWeaponActor* LocalLastWeapon = NULL;
	if(LastWeapon != NULL)
	{
		LocalLastWeapon = LastWeapon;
	}
	if(NewWeapon)
	{
		NewWeapon->SetOwningPawn(this);
		NewWeapon->OnEquip(LastWeapon);
	}
	
}

void ABasicCharacter::SpawnDefaultInventory()
{
	int32 NumWeaponClasses = DefaultInventoryClasses.Num();
	FActorSpawnParameters SpawnInfo;
	UWorld* World = GetWorld();
	if (NumWeaponClasses > 0)
	{
		AMyTestWeaponActor* NewWeapon = World->SpawnActor<AMyTestWeaponActor>(DefaultInventoryClasses[0], SpawnInfo);
		AddWeapon(NewWeapon);
	}
	if(Inventory.Num()>0)
	{
		EquipWeapon(Inventory[0]);
	}
}
//��
//====================================================

// Called when the game starts or when spawned
void ABasicCharacter::BeginPlay()
{
	Super::BeginPlay();
}

//================================================
//������, ����
void ABasicCharacter::OnHit(float DamageTaken, FDamageEvent const& DamageEvent, APawn* PawnInstigator,
	AActor* DamageCauser)
{
	PlayAnimMontage(BeHit_AnimMontage);
	if(DamageTaken > 0)
	{
		ApplyDamageMomentum(DamageTaken, DamageEvent, PawnInstigator, DamageCauser);
	}
}

void ABasicCharacter::Die(float KillingDamage, FDamageEvent const& DamageEvent, AController* Killer,
	AActor* DamageCauser)
{
	myHealth = FMath::Min(0.f, myHealth);

	UDamageType const* const DamageType = DamageEvent.DamageTypeClass ? Cast<const UDamageType>(DamageEvent.DamageTypeClass->GetDefaultObject()) : GetDefault < UDamageType >();

	Killer = GetDamageInstigator(Killer, *DamageType);

	GetWorldTimerManager().ClearAllTimersForObject(this);

	if(GetCapsuleComponent())
	{
		GetCapsuleComponent()->BodyInstance.SetCollisionEnabled(ECollisionEnabled::NoCollision);
		GetCapsuleComponent()->BodyInstance.SetResponseToChannel(ECC_Pawn, ECR_Ignore);
		GetCapsuleComponent()->BodyInstance.SetResponseToChannel(ECC_PhysicsBody, ECR_Ignore);
	}
	if (GetCharacterMovement())
	{
		GetCharacterMovement()->StopMovementImmediately();
		GetCharacterMovement()->DisableMovement();
	}
	if (Controller != NULL)
	{
		Controller->UnPossess();
	}

	GEngine->AddOnScreenDebugMessage(-1, 5, FColor::Blue, FString::Printf(TEXT("DEAD")));

	float DeathAnimDuration = PlayAnimMontage(Death_AnimMontage);
	FTimerHandle TimerHandle;
	GetWorldTimerManager().SetTimer(TimerHandle, this, &ABasicCharacter::DeathAnimationEnd, DeathAnimDuration, false);
	
}

void ABasicCharacter::DeathAnimationEnd()
{
	this->SetActorHiddenInGame(true);
	SetLifeSpan(.1f);
}

//��
//================================================

// Called every frame
void ABasicCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ABasicCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}
//================================================
//������, ����
float ABasicCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator,
	AActor* DamageCauser)
{
	const float myGetDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	if (myHealth > 0)
	{
		myHealth -= myGetDamage;
	}
	if (myHealth <= 0)
	{
		Die(myGetDamage, DamageEvent, EventInstigator, DamageCauser);
	}
	else
	{
		PlayAnimMontage(BeHit_AnimMontage);
		GEngine->AddOnScreenDebugMessage(-1, 5, FColor::Red, FString::Printf(TEXT("Hit!! HP is : %f"), myHealth));
		OnHit(myGetDamage, DamageEvent, EventInstigator ? EventInstigator->GetPawn() : NULL, DamageCauser);
	}
	
	return myGetDamage;
}
//��
//================================================

void ABasicCharacter::Attack_Melee()
{
	if (!isDuringAttack) 
	{
		if (ComboAttack_Num < 3)
		{
			int tmp_num = rand() % 3 + 1;
			FString PlaySection = "Attack_" + FString::FromInt(tmp_num);
			PlayAnimMontage(Attack_AnimMontage, 1.0f, FName(*PlaySection));

			ComboAttack_Num++;
			

			isDuringAttack = true;
		}
		else
		{
			PlayAnimMontage(Attack_AnimMontage, 1.0f, FName("Attack_4"));
			isDuringAttack = true;
			ComboAttack_Num = 0;
		}
	}

}

void ABasicCharacter::Attack_Melee_End()
{
	isDuringAttack = false;
}

//================================================
//������, ����
//void ABasicCharacter::ShowFx()
//{
//	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), HitFX, GetActorLocation());
//}
//��
//================================================

