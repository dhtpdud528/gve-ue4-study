// Copyright Epic Games, Inc. All Rights Reserved.


#include "MyProjectGameModeBase.h"
#include "Engine/DataTable.h"
#include "Blueprint/UserWidget.h"

void AMyProjectGameModeBase::BeginPlay()
{
    Super::BeginPlay();

    //뷰포트에 뿌려주는 과정
    CurrentWidget = CreateWidget<UUserWidget>(GetWorld(), HUDWidget);
    CurrentWidget->AddToViewport();
}