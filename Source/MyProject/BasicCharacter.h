// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "BasicCharacter.generated.h"

UCLASS()
class MYPROJECT_API ABasicCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ABasicCharacter();
	//================================================
	//public: 부분에 추가
	//무기 장착
	USkeletalMeshComponent* GetSpesificPawnMesh()const;

	FName GetWeaponAttachPoint()const;

	void EquipWeapon(class AMyTestWeaponActor* Weapon);
	//끝
	//================================================

	//================================================
	//public: 부분에 추가
	//데미지, 죽음
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = MyState)
	float myHealth;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = MyState)
	float myMaxHealth;
	////끝
	//================================================
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//================================================
	//protected: 부분에 추가
	//무기 장착
	UPROPERTY(EditDefaultsOnly, Category = Inventory)
	FName WeaponAttachPoint;

	TArray <class AMyTestWeaponActor*> Inventory;

	class AMyTestWeaponActor* Currentweapon;

	void AddWeapon(class AMyTestWeaponActor* Weapon);
	void SetCurrentWeapon(class AMyTestWeaponActor* NewWeapon, class AMyTestWeaponActor* LastWeapon);

	void SpawnDefaultInventory();

	UPROPERTY(EditDefaultsOnly, Category = Inventory)
	TArray<TSubclassOf<class AMyTestWeaponActor>> DefaultInventoryClasses;
	//끝
	//================================================
	
	//================================================
	//protected: 부분에 추가
	//데미지, 죽음
	virtual void OnHit(float DamageTaken, struct FDamageEvent const& DamageEvent, class APawn* PawnInstigator, class AActor* DamageCauser);

	virtual  void Die(float KillingDamage, struct FDamageEvent const& DamageEvent, AController* Killer, class AActor* DamageCauser);
	
	void DeathAnimationEnd();
	//끝
	//================================================
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	
	UPROPERTY(EditDefaultsOnly, Category = Pawn)
	UAnimMontage* Attack_AnimMontage;

	//================================================
	//public: 부분에 추가
	//데미지, 죽음
	UPROPERTY(EditDefaultsOnly, Category = Pawn)
	UAnimMontage* BeHit_AnimMontage;
	
	UPROPERTY(EditDefaultsOnly, Category = Pawn)
	UAnimMontage* Death_AnimMontage;

	virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
	
	////끝
	//================================================

	void Attack_Melee();
	void Attack_Melee_End();
	//================================================
	//public: 부분에 추가
	//데미지, 죽음
	//void ShowFX();;

	//UPROPERTY(EditDefaultsOnly, Category = "MyFX")//
	//	UParticleSystem* HitFX;//
	////끝
	//================================================

	bool isDuringAttack = true;

	int32 ComboAttack_Num = 0;
};
