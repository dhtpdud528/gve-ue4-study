// Fill out your copyright notice in the Description page of Project Settings.


#include "MyTestActor.h"

#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "BasicCharacter.h"
#include "Engine.h"
#include "MyTestCharacter.h"

#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"

// Sets default values
AMyTestActor::AMyTestActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	//GEngine->AddOnScreenDebugMessage(-1, 5.0, FColor::Yellow, TEXT("Hello world"));
	//UE_LOG(LogTemp, Warning, TEXT("TEST"));

	//Subobject 메쉬
	mStaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Obj"));
	RootComponent = mStaticMesh;

	//CreateDefaultSubobject : 클래스를 인스턴스화 시키는 메소드 
	//이 구문은 USphereComponent타입으로 CollisionSphere라는 이름을 갖는 객체가 만들어지는 것 
	CollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionSphere"));
	CollisionSphere->InitSphereRadius(100.0f);
	CollisionSphere->SetupAttachment(RootComponent);
	//AddDynamic(상황이 되었을때 자동으로 실행)
	CollisionSphere->OnComponentBeginOverlap.AddDynamic(this, &AMyTestActor::OnOverlapBegin);

	//생성자에서만 사용 가능
	//언리얼 오브젝트 인스턴스들이 공유해서 사용하는 자원이므로 FObjectFinder 객체에 static 키워드를 줌
	//에셋 타입 정보를 가져올때 사용한다.
	static ConstructorHelpers::FObjectFinder<UParticleSystem>ParticleAsset(TEXT("/Game/StarterContent/Particles/P_Explosion.P_Explosion"));
	ParticleFX = ParticleAsset.Object;
}

// Called when the game starts or when spawned
void AMyTestActor::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AMyTestActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

//OtherActor 다른 액터랑 만났을경우
//IsA 로 AMyTestCharacter가 맞을때 true
//디버그하고 지워져라
void AMyTestActor::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if(OtherActor->IsA(AMyTestCharacter::StaticClass()))
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.0, FColor::Red, TEXT("Collision Touch!!"));

		//위치에 뿌려주는 함수
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ParticleFX, GetActorLocation());
		Destroy();
	}
}
