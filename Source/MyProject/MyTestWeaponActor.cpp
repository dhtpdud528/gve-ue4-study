// Fill out your copyright notice in the Description page of Project Settings.


#include "MyTestWeaponActor.h"

//================================================
//데미지, 죽음
#include "Kismet/GameplayStatics.h"
//끝
//================================================

// Sets default values
AMyTestWeaponActor::AMyTestWeaponActor(const class FObjectInitializer& ObjectInitializer):Super(ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	WeaponMesh = ObjectInitializer.CreateDefaultSubobject<USkeletalMeshComponent>(this, TEXT("WeaponMesh"));
	WeaponMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	RootComponent = WeaponMesh;

	WeaponCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("WeaponCollision"));
	WeaponCollision->SetBoxExtent(FVector(5, 5, 5));
	//SetupAttachment 도 가능
	WeaponCollision->AttachTo(WeaponMesh);

}
void AMyTestWeaponActor::SetOwningPawn(ABasicCharacter* NewOwner)
{
	if(MyPawn != NewOwner)
	{
		MyPawn = NewOwner;
	}
}

void AMyTestWeaponActor::AttachMeshToPawn()
{
	if(MyPawn)
	{
		USkeletalMeshComponent* PawnmeMesh = MyPawn->GetSpesificPawnMesh();
		FName AttachPoint = MyPawn->GetWeaponAttachPoint();
		WeaponMesh->AttachTo(PawnmeMesh, AttachPoint);
	}
}

void AMyTestWeaponActor::OnEquip(const AMyTestWeaponActor* LastWeapon)
{
	AttachMeshToPawn();
}

// Called when the game starts or when spawned
void AMyTestWeaponActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMyTestWeaponActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}
//================================================
//데미지, 죽음
void AMyTestWeaponActor::NotifyActorBeginOverlap(AActor* OtherActor)
{
	if(OtherActor -> IsA(AActor::StaticClass())&& MyPawn->isDuringAttack && OtherActor != MyPawn)
	{
		UGameplayStatics::ApplyDamage(OtherActor, 30, NULL, this, UDamageType::StaticClass());

		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), HitFX, GetActorLocation());
	}
}
//끝
//================================================

