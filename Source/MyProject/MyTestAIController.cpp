#include "MyTestAIController.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISenseConfig_Sight.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "MyTestCharacter.h"



AMyTestAIController::AMyTestAIController() {

	PrimaryActorTick.bCanEverTick = true;

	sightConfig = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("Sight_Config"));
	SetPerceptionComponent(*CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("Perception Component")));


	// 인지 반경
	sightConfig->SightRadius = aiSightRadius;
	// 인지 손실 반경
	sightConfig->LoseSightRadius = aiLoseSightRadius;
	// 시야각
	sightConfig->PeripheralVisionAngleDegrees = aiFieldOfView;
	// 인지 후 풀리는 시간
	sightConfig->SetMaxAge(aiSightAge);


	// 적군, 아군, 중립에 대한 탐색 여부
	sightConfig->DetectionByAffiliation.bDetectEnemies = true;
	sightConfig->DetectionByAffiliation.bDetectFriendlies = true;
	sightConfig->DetectionByAffiliation.bDetectNeutrals = true;
	// 인지에 대한 컴포넌트 설정
	// 위에서 설정한 인지 정보를 설정
	GetPerceptionComponent()->SetDominantSense(*sightConfig->GetSenseImplementation());
	GetPerceptionComponent()->ConfigureSense(*sightConfig);
	// 인지에 대한 이벤트를 델리게이트로 설정
	sense.BindUFunction(this, "OnPawnDetercted");
	GetPerceptionComponent()->OnPerceptionUpdated.Add(sense);
	//GetPerceptionComponent()->OnPerceptionUpdated.AddDynamic(this, &AMyTestAIController::OnPawnDetercted);
}



void AMyTestAIController::BeginPlay() {

	Super::BeginPlay();

	// 현재 컴포넌트 null인지 확인
	if (GetPerceptionComponent() != nullptr) {
		//UE_LOG(LogTemp, Warning, TEXT("System Set"));
	}
	else {
		//UE_LOG(LogTemp, Warning, TEXT("No Component"));
	}
}

void AMyTestAIController::OnPossess(APawn* pawn) {

	Super::OnPossess(pawn);

}


void AMyTestAIController::Tick(float deltaSeconds) {

	Super::Tick(deltaSeconds);
	AMyTestCharacter* character = Cast<AMyTestCharacter>(GetPawn());
	if (bIsPlayerDetected == true) {
		AMyTestCharacter* player = Cast<AMyTestCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
		MoveToActor(player, 5.0f);
	}
}


FRotator AMyTestAIController::GetControlRotation() const {

	if (GetPawn() == nullptr)
		return FRotator(0.f, 0.f, 0.f);

	return FRotator(0.f, GetPawn()->GetActorRotation().Yaw, 0.f);
}


void AMyTestAIController::OnPawnDetercted(TArray<AActor*> detectedPawns) {
	//if()
	for (size_t i = 0; i < detectedPawns.Num(); i++) {
		distanceToPlayer = GetPawn()->GetDistanceTo(detectedPawns[i]);
	}

	bIsPlayerDetected = true;
}
